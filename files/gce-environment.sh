#!/bin/sh

echo "GCE_INTERNAL_IP=$(curl -sH "Metadata-Flavor:Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/ip)" >> /etc/environment
echo "GCE_EXTERNAL_IP=$(curl -sH "Metadata-Flavor:Google" http://metadata/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)" >> /etc/environment
