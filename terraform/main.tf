module "stolon" {
  source                = "./stolon"
  instances             = 4
  commit                = "${var.commit}"
  project               = "${var.project}"
  zone                  = "${var.zone}"
  ssh_user              = "${var.ssh_user}"
  ssh_private_key_path  = "${var.ssh_private_key_path}"
}
