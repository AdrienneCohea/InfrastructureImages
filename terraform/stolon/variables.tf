variable "commit" {}
variable "project" {}
variable "zone" {}
variable "ssh_user" {}
variable "ssh_private_key_path" {}
variable "instances" {}
