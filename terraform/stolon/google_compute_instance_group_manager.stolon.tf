resource "google_compute_instance_group_manager" "stolon" {
  name                  = "stolon"
  project               = "${var.project}"

  base_instance_name    = "stolon-keeper"
  instance_template     = "${google_compute_instance_template.stolon.self_link}"
  zone                  = "${var.zone}"

  target_size           = "${var.instances}"
  update_strategy       = "ROLLING_UPDATE"

  rolling_update_policy {
    type = "PROACTIVE"
    minimal_action = "REPLACE"
    max_surge_percent = 20
    max_unavailable_fixed = 1
    min_ready_sec = 12
  }
}
