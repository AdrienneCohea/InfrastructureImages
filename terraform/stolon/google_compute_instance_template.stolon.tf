resource "google_compute_instance_template" "stolon" {
  name                  = "stolon-${var.commit}"
  project               = "${var.project}"
  description           = "This template is used to create Stolon-PostgreSQL instances."

  tags = [
    "allow-health-checks",
    "consul-server"
  ]

  instance_description  = "Stolon-PostgreSQL"
  machine_type          = "n1-standard-1"

  disk {
    source_image        = "${var.project}/stolon"
    boot                = true
  }

  network_interface {
    network = "default"

    access_config {}
  }

  service_account {
    scopes = [
      "userinfo-email",
      "compute-ro",
      "storage-ro"
    ]
  }
}
