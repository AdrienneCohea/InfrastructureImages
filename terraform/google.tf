provider "google-beta" {
  credentials = "${file(var.account_file_path)}"
  project     = "${var.project}"
  region      = "${var.region}"
}
