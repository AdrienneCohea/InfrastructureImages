variable "ssh_user" {}
variable "ssh_private_key_path" {}
variable "account_file_path" {}
variable "project" {}
variable "region" {}
variable "zone" {}
variable "commit" {}
