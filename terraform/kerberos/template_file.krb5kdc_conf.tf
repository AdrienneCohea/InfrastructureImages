data "template_file" "krb5kdc_conf" {
  template = "${file("${path.module}/templates/krb5kdc.conf")}"

  vars {
    realm  = "C.${upper(var.project)}.INTERNAL"
  }
}
