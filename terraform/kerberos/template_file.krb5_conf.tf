data "template_file" "krb5_conf" {
  template = "${file("${path.module}/templates/krb5.conf")}"

  vars {
    realm               = "C.${upper(var.project)}.INTERNAL"
    internal_dns_domain = "c.${var.project}.internal"
    #kdc_list            = "${join("", formatlist("%s", ["kdc1", "kdc2", "kdc3"]))}"
    kdc_list            = "#whatever"
  }
}
