resource "google_compute_instance" "kdc" {
  count        = "${var.servers}"
  name         = "kdc${count.index + 1}"
  machine_type = "g1-small"
  zone         = "us-west1-a"

  tags = ["kerberos-kdc"]

  boot_disk {
    initialize_params {
      image = "adrienne-devops/kerberos"
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection { 
    user = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key_path)}"
  }

  provisioner "file" {
    content       = "${data.template_file.krb5_conf.rendered}"
    destination   = "/tmp/krb5.conf"
  }

  provisioner "file" {
    content       = "${data.template_file.krb5kdc_conf.rendered}"
    destination   = "/tmp/krb5kdc.conf"
  }

  provisioner "remote-exec" {
    inline = [
        "sudo mv /tmp/krb5.conf /etc/krb5.conf",
        "sudo mv /tmp/krb5kdc.conf /var/kerberos/krb5kdc/kdc.conf",
    ]      
  }
}
