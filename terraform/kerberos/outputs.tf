output "krb5_conf" {
  value = "${data.template_file.krb5_conf.rendered}"
}

output "krb5kdc_conf" {
  value = "${data.template_file.krb5kdc_conf.rendered}"
}
