#!/bin/bash
set -e

sudo mv /tmp/stolon-keeper.service /lib/systemd/system/stolon-keeper.service
sudo mv /tmp/stolon-sentinel.service /lib/systemd/system/stolon-sentinel.service
sudo mv /tmp/stolon-proxy.service /lib/systemd/system/stolon-proxy.service

sudo systemctl daemon-reload

cd /tmp
wget -q https://github.com/sorintlab/stolon/releases/download/v${STOLON_VERSION}/stolon-v${STOLON_VERSION}-linux-amd64.tar.gz
tar zxf stolon-v${STOLON_VERSION}-linux-amd64.tar.gz
rm -f stolon-v${STOLON_VERSION}-linux-amd64.tar.gz
sudo mv stolon-v${STOLON_VERSION}-linux-amd64/bin/* /usr/bin
rm -rf stolon-v${STOLON_VERSION}-linux-amd64

stolonctl version
stolon-keeper --version
stolon-sentinel --version
stolon-proxy --version

sudo systemctl enable stolon-sentinel
sudo systemctl enable stolon-keeper
sudo systemctl enable stolon-proxy

sync
