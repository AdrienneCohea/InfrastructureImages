#!/bin/bash
set -e

DEBIAN_FRONTEND=noninteractive

sudo apt upgrade -y
sudo apt autoremove -y
sync
