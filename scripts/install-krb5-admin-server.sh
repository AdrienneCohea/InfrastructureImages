#!/bin/bash
set -e

echo "[+] Installing packages: krb5-server krb5-libs"
sudo yum install --assumeyes krb5-server krb5-libs
echo "[+] Stopping service: kadmin"
sudo systemctl stop kadmin || true
echo "[+] Stopping service: krb5kdc"
sudo systemctl stop krb5kdc || true
echo "[+] Removing default systemd units and configuration files"
sudo rm -f /usr/lib/systemd/system/kadmin.service /usr/lib/systemd/system/krb5kdc.service /etc/krb5.conf /var/kerberos/krb5kdc/kdc.conf /var/kerberos/krb5kdc/kadm5.acl
echo "[+] Replacing systemd units"
sudo mv /tmp/kadmin.service /usr/lib/systemd/system/kadmin.service
sudo mv /tmp/krb5kdc.service /usr/lib/systemd/system/krb5kdc.service
echo "[+] Enabling path-based activation"
sudo mv /tmp/reload-kerberos.path /usr/lib/systemd/system/reload-kerberos.path
sudo mv /tmp/reload-kerberos.service /usr/lib/systemd/system/reload-kerberos.service
sudo systemctl daemon-reload
sudo systemctl enable reload-kerberos.path
sudo systemctl enable kadmin
sync
