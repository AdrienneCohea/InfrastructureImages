#!/bin/bash
set -e

export DEBIAN_FRONTEND=noninteractive

wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"

sudo apt update
sudo apt install -y -q postgresql-9.6
sudo systemctl stop postgresql
sudo systemctl disable postgresql

sync
