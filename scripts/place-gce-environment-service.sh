#!/bin/bash
set -e

chmod +x /tmp/gce-environment.sh
sudo mv /tmp/gce-environment.service /lib/systemd/system/gce-environment.service
sudo mv /tmp/gce-environment.sh /usr/bin/gce-environment.sh
sudo systemctl daemon-reload
sync
