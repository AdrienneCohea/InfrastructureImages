#!/bin/bash
set -e

CONSUL_VERSION="1.4.0"

wget -q https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
unzip -qq consul_${CONSUL_VERSION}_linux_amd64.zip
sudo mv consul /usr/bin
sudo mkdir -p /etc/consul

PROJECT_NAME=$(curl -H "Metadata-Flavor: Google" http://metadata.google.internal/computeMetadata/v1/project/project-id)

cat << EOF | sudo tee /etc/consul/config.json
{
    "server": true,
    "ui": true,
    "bootstrap_expect": 3,
    "bind_addr": "{{GetPrivateIP}}",
    "addresses": {
        "http": "0.0.0.0"
    },
    "protocol": 3,
    "data_dir": "/var/lib/consul",
    "retry_join": ["provider=gce project_name=${PROJECT_NAME} tag_value=consul-server"]
}
EOF

sudo mv /tmp/consul.service /lib/systemd/system/consul.service

sudo systemctl daemon-reload
sudo systemctl reenable consul
sync
